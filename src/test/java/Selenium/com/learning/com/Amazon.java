package Selenium.com.learning.com;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Amazon {
	
	static ChromeDriver driver;

	@Test
	public void checkPNR() throws InterruptedException, IOException {
		
	
		
		try {

		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		//options.setHeadless(true); // Set Chrome option
		driver = new ChromeDriver();
		driver.get("https://www.amazon.in/");
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//span[text()='Hello, sign in']")).click();
		driver.findElement(By.xpath("//input[@type='email']")).sendKeys("yoguram@gmail.com");
		driver.findElement(By.xpath("//input[@id='continue']")).click();
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Ramsiva@31");
		driver.findElement(By.xpath("//input[@id='signInSubmit']")).click();
		driver.findElement(By.xpath("//span[text()='Hello, Ramachandran']")).click();
		driver.findElement(By.xpath("//h2[contains(text(),'Your Orders')]")).click();
		driver.findElement(By.xpath("//a[contains(@href,'402-1440964-0661143')]")).click();
		driver.findElement(By.xpath("//button[text()='Get latest PNR status']")).click();
		Thread.sleep(5000);
		TakesScreenshot scrShot = ((TakesScreenshot) driver);
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		File DestFile = new File("C:\\Users\\Syscloud-Emp\\.jenkins\\workspace\\Amazon pnr\\1.jpg");
		FileUtils.copyFile(SrcFile, DestFile);
		String text = driver.findElement(By.xpath("//div[text()='Current Status']/parent::div/following::div/div[3]"))
				.getText();
		System.out.println(text + " Subramanian");
		driver.findElement(By.xpath("//span[text()='Hello, Ramachandran']")).click();
		driver.findElement(By.xpath("//h2[contains(text(),'Your Orders')]")).click();
		driver.findElement(By.xpath("//a[contains(@href,'405-3028933-0737951')]")).click();
		driver.findElement(By.xpath("//button[text()='Get latest PNR status']")).click();
		Thread.sleep(5000);
		File SrcFile1 = scrShot.getScreenshotAs(OutputType.FILE);
		File DestFile1 = new File("C:\\Users\\Syscloud-Emp\\.jenkins\\workspace\\Amazon pnr\\2.jpg");
		FileUtils.copyFile(SrcFile1, DestFile1);
		String text1 = driver.findElement(By.xpath("//div[text()='Current Status']/parent::div/following::div/div[3]"))
				.getText();
		System.out.println(text1 + " Poo");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			driver.quit();
		}
		

	}

}
